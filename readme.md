# autocad.trusted.locations

This simple command-line tool adds and removes provided paths to the windows registry.

You can download the binarys in the [download](https://bitbucket.org/Seth007/autocad.trusted.locations/downloads/) section of this repository.

Usage:
`-d, --debug[optional]... Set whether to show debug information or not. When omitted equals FALSE.`

`-a, --arguments[optional]... Set whether to show debug information about arguments parsed on command-line. When omitted equals FALSE.`

`-h, --help[optional]... See command-line arguments help. When omitted equals FALSE.`

`-p, --Paths... Set the paths to the folders u want to add to the trusted locations.`

Example: `-p "c:\example\" -p "c:\example2\..."`

Set the paths to the folders u want to add to the trusted locations, they should not contain the separator character `";"`. You can provide multiple paths by setting the argument multiple times. You can make use of the tree dot notation to trust recursive inside the folder.

`-t, --TaskType... What do you want the application to do, add, or remove the registry values.`

Example: `-t Add` or `-t Remove`