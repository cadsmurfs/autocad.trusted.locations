﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AutocadTrustedLocations
{
    public static class RegistryHelper
    {
        #region Private Fields

        /// <summary>
        /// @"Software\Autodesk\AutoCAD\"
        /// </summary>
        private const string Prefix = @"Software\Autodesk\AutoCAD\";

        /// <summary>
        /// @"\Profiles"
        /// </summary>
        private const string ProfilesSuffix = @"\Profiles";

        private const string TrustedPathName = @"TRUSTEDPATHS";

        /// <summary>
        /// @"\Variables"
        /// </summary>
        private const string VariablesSuffix = @"\Variables";

        /// <summary>
        /// CreateNameTrustedLocations = s =&gt; $"{Prefix}{s}";
        /// </summary>
        private static readonly Func<string, string> CreateNameTrustedLocations = s => $"{Prefix}{s}";

        #endregion

        #region Public Methods

        public static bool CreateTrustedPaths(IEnumerable<string> pathsToAppend, Dictionary<string, string> SoftwareProductStringPairs, out string error)
        {
            var success = false;
            error = string.Empty;
            foreach (var softwareProduct in SoftwareProductStringPairs)
            {
                if (FuncSetValuesAndLog(softwareProduct, pathsToAppend, out error))
                    success = true;
            }

            return success;
        }

        public static void RemoveTrustedPaths(IEnumerable<string> pathsToRemove, Dictionary<string, string> SoftwareProductStringPairs)
        {
            // ETXRA INFO
            // Info: Delete the unwanted server path's from TRUSTEDPATHS: see
            // https://bitbucket.org/Seth007/ifb-iap/issues/17/we-should-remove-server-from-trusted
            // for more info.

            // First get all profile paths
            var pathToProfiles = GetPathToProfiles(SoftwareProductStringPairs);

            // If no profiles variable folders have been found, return home!
            var toProfiles = pathToProfiles as IList<string> ?? pathToProfiles.ToList();
            if (!toProfiles.Any()) return;

            // Look for the key to make the adjustments to in all locations found.
            foreach (var s in toProfiles)
            {
                Debug.Assert(!string.IsNullOrWhiteSpace(s));

                using (var regCurUser = Registry.CurrentUser)
                {
                    Debug.Assert(regCurUser != null);

                    using (var regVariables = regCurUser.OpenSubKey(s + VariablesSuffix, true))
                    {
                        // If it does not exits is will be null ...
                        var currentValue = regVariables?.GetValue(TrustedPathName);

                        // If the value is empty, we continue
                        if (string.IsNullOrWhiteSpace(currentValue as string)) continue;

                        // Remove any occurrences of strings we want to remove.

                        // split current value's on delimiter
                        var values = ((string)currentValue).Split(";".ToCharArray());

                        // If it contains no value's we can continue
                        if (!values.Any()) continue;

                        // Else we loop over them and remove the once's we do not
                        // want anymore.
                        var newStringValue = new List<string>();
                        foreach (var value in values)
                        {
                            var found = false;
                            foreach (var stringToRemove in pathsToRemove)
                            {
                                if (!stringToRemove.Equals(value, StringComparison.CurrentCultureIgnoreCase)) continue;

                                found = true; break;
                            }
                            if (!found) newStringValue.Add(value);
                        }

                        var joined = !newStringValue.Any() ? "" : string.Join(";", newStringValue.ToArray());

                        // Only write if changes are actually necessary
                        if (currentValue.Equals(joined)) continue;

                        regVariables.SetValue(TrustedPathName, joined, RegistryValueKind.String);
                        var x = string.IsNullOrWhiteSpace(joined) ? "null" : joined;

                        var debugMessage =
                            $"[{regVariables.Name}{TrustedPathName}] change FROM:---->[{currentValue}]{Environment.NewLine}TO:---->{x}";
                        Debug.Print(debugMessage);
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        private static bool CreateOrAppendTrustedLocationsInAllProfiles(string autoCadDependedPrefix, string stringToAppend, out string error)
        {
            // here we set all the trusted locations we need so that C3D does not
            // complain when opening and loading our application, we do not know
            // what profiles have been created, so we need to add it to all profiles.

            //http://knowledge.autodesk.com/support/autocad/learn-explore/caas/CloudHelp/cloudhelp/2016/ENU/AutoCAD-Core/files/GUID-2FB4611D-F141-48D5-9B6E-460EB59351AF-htm.html

            /*
             Valid strings include the following:

            When TRUSTEDPATHS is set to one or more folder paths in quotes and separated by semicolons, the previously listed file types are loaded from the specified folders.
            When TRUSTEDPATHS is set to "" (an empty string) or "." (a period), there are no trusted folder paths in addition to the implicitly trusted ones.
            When TRUSTEDPATHS includes a folder that ends with \... (backslash and three dots), all of its sub folders are also trusted.
             */

            var reg = new RegistryManager(autoCadDependedPrefix);

            return reg.TryAppendKeyValueToAllSubKeys(@"Profiles", @"Variables", TrustedPathName, stringToAppend,
                ";" + stringToAppend, out error);
        }

        private static bool FuncSetValuesAndLog(KeyValuePair<string, string> keyValuePair, IEnumerable<string> pathsToAppend, out string _error)
        {
            var successfull = false;
            _error = string.Empty;
            // Add trusted locations to the profiles in registry
            for (int i = 0; i < pathsToAppend.Count(); i++)
            {
                try // for each path, we can fail
                {
                    successfull = CreateOrAppendTrustedLocationsInAllProfiles(CreateNameTrustedLocations(keyValuePair.Value),
pathsToAppend.ElementAt(i), out _error);
                }
                catch (Exception ex)
                {
                    Debug.Print(ex.Message);
                }
            }

            return successfull;
        }

        /// <summary>
        /// This function will return a list of all profile paths available in
        /// all know AutoCad product registry's.
        /// </summary>
        /// <returns> A list of path's relative to the current user. </returns>
        private static IEnumerable<string> GetPathToProfiles(Dictionary<string, string> SoftwareProductStringPairs)
        {
            var pathToProfiles = new List<string>();

            foreach (var result in SoftwareProductStringPairs.Select(
                softwareProductStringPair => RegistryManager.GetPathOfChildItemsFromCurrentUser(
                    $"{Prefix}{softwareProductStringPair.Value}{ProfilesSuffix}")).Where(result => result != null))
            {
                pathToProfiles.AddRange(result);
            }
            return pathToProfiles;
        }

        #endregion
    }

    /// <summary>
    /// This class is used to manipulate the registry use by all IAP applications.
    /// </summary>
    internal class RegistryManager
    {
        #region Private Fields

        /// <summary>
        /// Removes all whitespace from end and start of string, as well as all
        /// occurrences of \ and /
        /// </summary>
        private static readonly Func<string, string> SanitizeInput = s => s.Trim().Trim("\\/".ToCharArray());

        #endregion

        #region Public Constructors

        /// <summary>
        /// Creates new instance of the registry manager.
        /// </summary>
        /// <param name="prefixOfKey"> Prefix of the path in registry. </param>
        public RegistryManager(string prefixOfKey)
        {
            PathPrefix = prefixOfKey;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// This prefix will get appended to the provided sub-key, default is "";
        /// </summary>
        public string PathPrefix { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// This will delete all provided key's from registry.
        /// </summary>
        /// <param name="deletelist"> 
        /// List of paths to the keys that need to be deleted, u do not need to
        /// include the path to the current user.
        /// </param>
        public static void DeleteKeysInCurrentUser(IEnumerable<string> deletelist)
        {
            Debug.Assert(deletelist != null, "Argument provided is null.", $"Name of object: \"{nameof(deletelist)}\"");

            foreach (var keyToDelete in deletelist)
            {
                using (var regCurUser = Registry.CurrentUser)
                {
                    regCurUser.DeleteSubKeyTree(keyToDelete, false);
                    Debug.WriteLine($"Key deleted: {regCurUser.Name}\\{keyToDelete}");
                    Debug.Print($"Key deleted: {regCurUser.Name}\\{keyToDelete}");
                }
            }
        }

        /// <summary>
        /// Returns the path of all keys that match the key-name provided in the collection.
        /// </summary>
        /// <param name="pathToKeyCollection"> 
        /// Path to the key-collection, relative to current user.
        /// </param>
        /// <param name="keyNamesToFind">      
        /// Names of keys to look for. The parameter will get trimmed of any
        /// occurrences of "//" leading and trailing.
        /// </param>
        /// <exception cref="ArgumentNullException"> 
        /// The key collection provided is null.
        /// </exception>
        /// <exception cref="Exception"> 
        /// A delegate callback throws an exception.
        /// </exception>
        public static IEnumerable<string> GetListOfKeysFromCollectionByKeyList(string pathToKeyCollection,
            List<string> keyNamesToFind)
        {
            var deletelist = new List<string>();

            Debug.Assert(keyNamesToFind != null, "The argument u provided should not be null.");
            Debug.Assert(keyNamesToFind.Count > 0, "U should at least provide one keyName for deletion.");
            Debug.Assert(!string.IsNullOrWhiteSpace(pathToKeyCollection),
                "U should provide a path to the key collection.");

            // Sanitize input
            keyNamesToFind = keyNamesToFind.Select(x => SanitizeInput(x)).ToList();

            using (var registryCurrentUser = Registry.CurrentUser)
            {
                // Collection key = the key that holds all profile related
                // registry key's.
                using (var keyCollection = registryCurrentUser.OpenSubKey(pathToKeyCollection))
                {
                    if (keyCollection == null) return null;

                    // Now read all underlying sub keys, and add them to the list.

                    deletelist.AddRange(keyCollection.GetSubKeyNames()
                        .SelectMany(potentialDeleteKeyName => keyNamesToFind,
                            (potentialDeleteKeyName, keyNameToDelete) => new { potentialDeleteKeyName, keyNameToDelete })
                        .Where(@t =>

                            // ReSharper disable once ExceptionNotDocumentedOptional
                            @t.keyNameToDelete.Equals(@t.potentialDeleteKeyName,
                                StringComparison.CurrentCultureIgnoreCase))
                        .Select(@t => pathToKeyCollection + "\\" + @t.keyNameToDelete));
                }
            }

            return deletelist;
        }

        /// <summary>
        /// Returns a list of paths (strings) from a given key in the registry.
        /// </summary>
        /// <param name="pathToKeyCollection"> 
        /// Relative (to the current user) path to the key (collection).
        /// </param>
        /// <param name="throwExceptions">     
        /// True for exception throwing, false swallows exceptions, if no
        /// exceptions should be thrown returns null when key was not found.
        /// </param>
        /// <returns> 
        /// A list of sub-key's full-paths relative to the current user, returns
        /// null when key was not found, and exception throwing is disabled.
        /// </returns>
        /// <exception cref="Exception"> 
        /// A delegate callback throws an exception.
        /// </exception>
        public static IEnumerable<string> GetPathOfChildItemsFromCurrentUser(string pathToKeyCollection,
            bool throwExceptions = false)
        {
            Debug.Assert(!string.IsNullOrWhiteSpace(pathToKeyCollection),
                "U should provide a path to the key collection.");
            try
            {
                pathToKeyCollection = SanitizeInput(pathToKeyCollection);

                using (var regCurUser = Registry.CurrentUser)
                {
                    using (var subKey = regCurUser.OpenSubKey(pathToKeyCollection))
                    {
                        if (subKey == null)
                            if (!throwExceptions)
                                return null;
                            else
                                throw new Exception($"The key ({pathToKeyCollection}) could not be found.");

                        var children = subKey.GetSubKeyNames();

                        var returnlist = children.Select(x => $"{pathToKeyCollection}\\{x}").ToList();

                        return returnlist;
                    }
                }
            }
            catch (Exception exception)
            {
                if (throwExceptions)
                    throw;

                Debug.WriteLine($"Exception was swallowed. {exception.Message}");
                Debug.Print($"Exception was swallowed. {exception.Message}");
                return null;
            }
        }

        /// <summary>
        /// Checks to see if a certain subKeyString exists.
        /// </summary>
        /// <param name="relativePathFromCurrentUser">  </param>
        public static bool KeyExistsInCurrentUser(string relativePathFromCurrentUser)
        {
            using (var regCurUser = Registry.CurrentUser)
            {
                using (var subKey = regCurUser.OpenSubKey(relativePathFromCurrentUser))
                {
                    // If this sub-key does not exist we do not throw an error.
                    return subKey != null;
                }
            }
        }

        /// <summary>
        /// Reads a key value from registry, and throws exceptions.
        /// </summary>
        /// <param name="subkey">          The subKeyString. </param>
        /// <param name="keyName">         The key-name. </param>
        /// <param name="throwExceptions"> True if u want exceptions thrown. </param>
        /// <exception cref="Exception"> Sub key not found.. </exception>
        public string ReadKeyFromCurrentUser(string subkey, string keyName, bool throwExceptions = true)
        {
            if (!string.IsNullOrEmpty(PathPrefix))
                subkey = PathPrefix + subkey;

            using (var regCurUser = Registry.CurrentUser)
            {
                using (var subKey = regCurUser.OpenSubKey(subkey))
                {
                    // If this sub-key does not exist we throw an error.
                    if (subKey == null)
                        if (throwExceptions)
                            throw new Exception(
                                $"Could not find the sub-key in the registry tree of the current user.: {subkey}.");
                        else return null;

                    var value = subKey.GetValue(keyName);

                    if (value != null) return value as string;

                    if (throwExceptions)
                        throw new Exception(
                            $"Could not read the value of the key, does it exist? KeyName: {keyName}");

                    return null;
                }
            }
        }

        /// <summary>
        /// This function will iterate over all keys within the provided
        /// subKeyString, then it searches for a key within that collection, and
        /// creates or appends a provided string to the value.
        /// </summary>
        /// <param name="subkey">            Name of key to iterate over. </param>
        /// <param name="deepSubKeyName">    
        /// Name of the nested key that holds the actual value to change.
        /// </param>
        /// <param name="keyName">           Name of the actual value </param>
        /// <param name="keyValueForNew">    Value if it does not yet exist </param>
        /// <param name="keyValueForAppend"> 
        /// Value if it already exists, it will get the value appended to it. But
        /// ONLY when it is not yet contained within the value.
        /// </param>
        /// <returns>  </returns>
        /// <exception cref="Exception"> 
        /// The key-name could not be found in the registry.
        /// </exception>
        // ReSharper disable once UnusedMethodReturnValue.Global
        public bool TryAppendKeyValueToAllSubKeys(string subkey, string deepSubKeyName, string keyName,
            string keyValueForNew, string keyValueForAppend, out string error)
        {
            if (!string.IsNullOrEmpty(PathPrefix))
                subkey = PathPrefix + @"\" + subkey;

            using (var regCurUser = Registry.CurrentUser)
            {
                using (var key = regCurUser.OpenSubKey(subkey))
                {
                    if (key == null)
                    { error = $"The key \"{keyName}\" could not be found in the registry."; return false; }

                    // Now read all underlying sub keys, and append the value key
                    // where needed
                    foreach (var subKeyName in key.GetSubKeyNames())
                    {
                        using (var childSubKey = key.OpenSubKey(subKeyName, true))
                        {
                            // If it does not exist, continue to next
                            if (childSubKey == null)
                                continue;

                            using (var deep = childSubKey.OpenSubKey(deepSubKeyName, true))
                            {
                                // If it does not exist, we need to create it
                                if (deep == null)
                                {
                                    using (var deepFallback = childSubKey.CreateSubKey(deepSubKeyName))
                                    {
                                        if (deepFallback == null)
                                            continue;
                                        deepFallback.SetValue(keyName, keyValueForNew, RegistryValueKind.String);
                                        Debug.Print($"{deepFallback.Name}\\{keyName} has been created. Value: [{keyValueForNew}]");
                                    }
                                }
                                else
                                {
                                    // Retrieve the initial value, if is does not
                                    // exist, the returned value will be null.
                                    var valueObj = deep.GetValue(keyName, null);

                                    if (valueObj == null)
                                        // key does not exist, so we can continue;
                                        continue;

                                    string value = valueObj.ToString();
                                    if (string.IsNullOrWhiteSpace(value))
                                    {
                                        // Sets the name/value pair on the
                                        // specified registry key, using the
                                        // specified registry data type. If the
                                        // specified key does not exist, it is created.
                                        deep.SetValue(keyName, keyValueForNew, RegistryValueKind.String);
                                        Debug.Print($"{deep.Name}\\{keyName} has been created. Value: [{keyValueForNew}]");
                                    }
                                    else
                                    {
                                        // Only append if it is not already in
                                        // the value
                                        if (value.Contains(keyValueForAppend)) continue;

                                        deep.SetValue(keyName, value + keyValueForAppend,
                                            RegistryValueKind.String);
                                        Debug.Print($"{deep.Name}\\{keyName} has been adjusted. Value: [{value + keyValueForAppend}]");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            error = string.Empty;
            return true;
        }

        // ReSharper disable once UnusedMethodReturnValue.Global
        /// <summary>
        /// Writes the registry key to the current user.
        /// </summary>
        /// <param name="subKeyString"> Sukey </param>
        /// <param name="valueName">    Key name </param>
        /// <param name="value">        Actual value as string </param>
        /// <returns> boolean for success. </returns>
        /// <exception cref="Exception"> 
        /// A delegate callback throws an exception.
        /// </exception>
        public bool WriteKeyToCurrentUser(string subKeyString, string valueName, string value)
        {
            if (!string.IsNullOrEmpty(PathPrefix))
                subKeyString = SanitizeInput(PathPrefix) + "\\" + SanitizeInput(subKeyString);

            using (var regCurUser = Registry.CurrentUser)
            {
                using (var subKey = regCurUser.OpenSubKey(subKeyString, true))
                {
                    if (subKey == null)
                    {
                        // When the subKeyString is null, it does not exist, so
                        // lets create it
                        var newSubKey = regCurUser.CreateSubKey(subKeyString);
                        newSubKey?.SetValue(valueName, value);
                        Debug.Print($"Key created: [{regCurUser.Name}\\{subKeyString}\\{valueName}] Value: [{value}]");
                        return true;
                    }

                    // If it is not null, its already created, so lets change te value.
                    subKey.SetValue(valueName, value);
                    Debug.Print($"Key value updated:[{regCurUser.Name}\\{subKeyString}\\{valueName}] Value: [{value}]");
                    return true;
                }
            }
        }

        // ReSharper disable once UnusedMethodReturnValue.Global
        /// <summary>
        /// Writes the registry key to the current user.
        /// </summary>
        /// <param name="subKeyString"> Sub-key name </param>
        /// <param name="valueName">    Key name </param>
        /// <param name="value">        Actual value as object </param>
        /// <returns> Boolean for success. </returns>
        /// <exception cref="Exception"> 
        /// A delegate callback throws an exception.
        /// </exception>
        public bool WriteKeyToCurrentUser(string subKeyString, string valueName, object value)
        {
            if (!string.IsNullOrEmpty(PathPrefix))
                subKeyString = SanitizeInput(PathPrefix) + "\\" + SanitizeInput(subKeyString);

            using (var regCurUser = Registry.CurrentUser)
            {
                using (var subKey = regCurUser.OpenSubKey(subKeyString, true))
                {
                    if (subKey == null)
                    {
                        // When the sub key is null, it does not exist, so lets
                        // create it
                        var newSubKey = regCurUser.CreateSubKey(subKeyString);
                        newSubKey?.SetValue(valueName, value);
                        Debug.Print($"Key created: [{regCurUser.Name}\\{subKeyString}\\{valueName}] Value: [{value}]");
                        return true;
                    }

                    // If it is not null, its already created, so lets change the value.
                    subKey.SetValue(valueName, value);
                    Debug.Print($"Key value updated:[{regCurUser.Name}\\{subKeyString}\\{valueName}] Value: [{value}]");
                    return true;
                }
            }
        }

        #endregion
    }
}