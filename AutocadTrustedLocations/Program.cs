﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLineParser.Arguments;
using CommandLineParser.Validation;

namespace AutocadTrustedLocations
{
    internal enum TaskTypeEnum
    {
        Add, Remove
    }

    internal class Program
    {
        #region Private Fields

        private static readonly Dictionary<string, string> SoftwareProductStringPairs = new Dictionary<string, string>
        {
            // 2013
            {"AutoCAD 2013", @"R19.0\ACAD-B001:409"},
            {"AutoCAD 2013 Civil 3D", @"R19.0\ACAD-B000:409"},

            // 2014
            // TODO: Find out what variables we need for 2014

            // 2015
            {"AutoCAD 2015", @"R20.0\ACAD-E001:409"},
            {"AutoCAD 2015 Civil 3D", @"R20.0\ACAD-E000:409"},

            // 2016
            {"AutoCAD 2016", @"R20.1\ACAD-F001:409"},
            {"AutoCAD 2016 Civil 3D", @"R20.1\ACAD-F000:409"},

            // 2017
            {"AutoCAD 2017", @"R21.0\ACAD-0001:409"},
            {"AutoCAD 2017 Civil 3D", @"R21.0\ACAD-0000:409"},

            // 2018
            {"AutoCAD 2018", @"R22.0\ACAD-1001:409"},
            {"AutoCAD 2018 Civil 3D", @"R22.0\ACAD-1000:409"},

            // 2019
            {"AutoCAD 2019", @"R23.0\ACAD-2001:409"},
            {"AutoCAD 2019 Civil 3D", @"R23.0\ACAD-2000:409"},
             // 2020
            {"AutoCAD 2020", @"R23.1\ACAD-3001:409"},
            {"AutoCAD 2020 Civil 3D", @"R23.1\ACAD-3000:409"},
             // 2021
            {"AutoCAD 2021", @"R24.0\ACAD-4101:409"},
            {"AutoCAD 2021 Civil 3D", @"R24.0\ACAD-4100:409"},
            {"AutoCAD 2021 Mechanical", @"R24.0\ACAD-4105:409"}
        };

        #endregion

        #region Private Enums

        private enum ExitCode : int
        {
            UnknowError = -1,
            Success = 0,
            ShowingHelp = 2,
            NoParameters = 3
        }

        #endregion

        #region Private Methods

        private static void Main(string[] args)
        {
            var parser = new CommandLineParser.CommandLineParser();
            var settings = new ProgramCommandLineSettings();
            parser.ExtractArgumentAttributes(settings);
            TaskTypeEnum? task = null;
            try
            {
                //? No need to parse argument if there are none.
                if (args == null || args.Count() == 0) { { parser.ShowUsage(); Environment.Exit((int)ExitCode.NoParameters); } }

                parser.ParseCommandLine(args);

                var y = settings.TaskType;
                if (!Enum.TryParse<TaskTypeEnum>(y, true, out var taskType))
                    throw new ArgumentException("Passed value for the TypeTask argument was not in a valid format");
                task = taskType;

                if (settings.Paths.Exists(x => x.Contains(";")))
                    throw new ArgumentException("Passed value for the Paths argument contains the separator \";\", witch is an illegal char.");
            }
            catch (Exception ex)
            {
                WrtMsg($"Command line arguments failed to parse: {ex.Message}");
                //! No return here, we need to show usage to user.
            }

            try
            {
                // Set the show help property, when something went wrong
                if (!parser.ParsingSucceeded && !task.HasValue) { settings.Help = true; }

                if (settings.Help) { parser.ShowUsage(); Environment.Exit((int)ExitCode.ShowingHelp); }

                if (settings.ShowArguments) parser.ShowParsedArguments();

                // DO WORK HERE

                //+ Add
                if (task.Value == TaskTypeEnum.Add)
                {
                    if (!RegistryHelper.CreateTrustedPaths(settings.Paths, SoftwareProductStringPairs, out string error))
                        WrtMsg($"Function failed ==> \"{error}\"");
                }
                //+ Remove
                else if (task.Value == TaskTypeEnum.Remove)
                    RegistryHelper.RemoveTrustedPaths(settings.Paths, SoftwareProductStringPairs);

                // All went OK, so return 1
                Environment.Exit((int)ExitCode.Success);
            }
            catch (InvalidDataException ex)
            {
                WrtMsg($"Function failed ==> \"{ex.Message}\"");
            }
            catch (Exception ex)
            {
                if (parser.ParsingSucceeded)
                    WrtMsg($"Unknown exception: [{settings.Paths}] ==> \"{ex.Message}\"");
                else
                    WrtMsg($"Unknown exception: ==> \"{ex.Message}\"");
            }

            // Something went wrong, return -1
            Environment.Exit((int)ExitCode.UnknowError);
        }

        private static void WrtMsg(string message)
        {
            Console.WriteLine($"{DateTime.Now.ToString("T")}:  {message}");
        }

        #endregion
    }

    //[DistinctGroupsCertification("e,o", "p,r")]
    internal class ProgramCommandLineSettings
    {
        // https://github.com/j-maly/CommandLineParser

        #region Public Properties

        [SwitchArgument('d', "debug", false, Description = "Set whether to show debug information or not. When omitted equals FALSE.")]
        public bool Debug { get; set; }

        [SwitchArgument('h', "help", false, Description = "See command-line arguments help. When omitted equals FALSE.")]
        public bool Help { get; set; }

        [ValueArgument(typeof(string), 'p', "Paths",
            Description = "Set the paths to the folders u want to add to the trusted locations.",
            FullDescription = "Set the paths to the folders u want to add to the trusted locations, they should not contain the separator character \";\". You can provide multiple paths by setting the argument multiple times. You can make use of the tree dot notation to trust recursive inside the folder.",
            Example = "-p \"c:\\example\\\" -p \"c:\\example2\\...\"",
            AllowMultiple = true,
            Optional = false)]
        public List<string> Paths { get; set; }

        [SwitchArgument('a', "arguments", false, Description = "Set whether to show debug information about arguments parsed on command-line. When omitted equals FALSE.")]
        public bool ShowArguments { get; set; }

        [ValueArgument(typeof(string), 't', "TaskType", Description = "What do you want the application to do, add, or remove the registry values.", Example = "-t Add/Remove", Optional = false, DefaultValue = "Add")]
        public string TaskType { get; set; }

        #endregion
    }
}